package com.clases;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(
		description = "Ejemplo del uso de anotaciones", 
		urlPatterns = { "/Anotaciones" }, 
		initParams = { 
		@WebInitParam(name = "Version", value = "1.0", description = "Numero de version")
		})

public class Anotaciones extends HttpServlet {

	private static final long serialVersionUID = 1L;
       

    public Anotaciones() {
        super();
    }

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		
		pw.println("<html><body>");
		pw.println("<p>Ejemplo con Anotaciones</p>");
		pw.println("</body></html>");
	}

	
}
