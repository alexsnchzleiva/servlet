package com.clases;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Parametros extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
	
    public Parametros() {
        super();
    }
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Enumeration<?> initParam = this.getInitParameterNames();
		String creador = this.getInitParameter("Creador");
		String version = this.getInitParameter("Version");
		
		PrintWriter pw = response.getWriter();
		pw.println("<html><head><title>Titulo</title></head>");
		pw.println("<body>");
		
		while(initParam.hasMoreElements()){
			pw.println("<p>" + initParam.nextElement() + "</p>");
		}
		
		pw.println("<p>" + creador + "</p>");
		pw.println("<p>" + version + "</p>");
		
		HttpSession miSesion = request.getSession(true);
		
		if(miSesion.getAttributeNames().hasMoreElements()){
			pw.println("<p>Si se ha creado la sesion</p>");
			pw.println("<p>" + miSesion.getAttribute("IDSESION") + "</p>");
			pw.println("<p>" + miSesion.getAttribute("NOMBRE") + "</p>");
		}
		else{
			pw.println("<p>No se ha creado la sesion</p>");
		}
		
		pw.println("</body>");
		pw.println("</html>");
		pw.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
