package com.clases;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class RemoveSession
 */
public class RemoveSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveSession() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession miSesion = request.getSession(true);
		miSesion.removeAttribute("NOMBRE");
		miSesion.removeAttribute("IDSESION");
		
		//response.sendRedirect("http://localhost:8080/MiPrimerServlet");
		
		PrintWriter pw = response.getWriter();
		pw.println("<html><head><title>Titulo</title></head>");
		pw.println("<body>");
		
		pw.println("<p>" + miSesion.getAttribute("IDSESION") + "</p>");
		pw.println("<p>" + miSesion.getAttribute("NOMBRE") + "</p>");
		
		pw.println("<a href='http://localhost:7001/MiPrimerServlet'>Volver</a>");
		
		pw.println("</body>");
		pw.println("</html>");
		pw.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
